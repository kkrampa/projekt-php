<?php

	$mysql_host = 'localhost';
	$port = '3306';
	$username = 'root';
	$password = '';
	$database = 'test';
	try{
		$pdo = new PDO('mysql:host='.$mysql_host.';dbname='.$database.';port='.$port, $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
	} catch(PDOException $e) {
		echo 'Połączenie nie mogło zostać utworzone.<br />';
	}
?>